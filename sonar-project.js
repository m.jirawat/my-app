const scanner = require("sonarqube-scanner");

scanner(
  {
    serverUrl: "http://localhost:9000",
    token: "sqp_b22ad117cddaecbaf56e6b6f6ee3d2b6162d37d6",
    options: {
      "sonar.projectName": "solo-app",
      "sonar.projectDescription": 'Description for "My App" project...',
      "sonar.sources": "src",
    },
  },
  () => process.exit()
);
